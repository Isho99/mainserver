const express = require('express');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');

var app = express();
var port =  process.env.PORT || 3000;

mongoose.connect('mongodb://localhost/arsabiServerOne');

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

const UserRoute = require('./server/routes/user');
const ImageRoute = require('./server/routes/images');
const MarketRoute = require('./server/routes/market');
const EliteRoute = require('./server/routes/elite');
const ReportRoute = require('./server/routes/report');
const AccountRoute = require('./server/routes/account');

app.use(express.static(__dirname + '/image'));

app.use(morgan('dev'))

app.use('/user', UserRoute);
app.use('/image', ImageRoute);
app.use('/market', MarketRoute);
app.use('/elite', EliteRoute);
app.use('/report', ReportRoute);
app.use('/account', AccountRoute);

app.listen(port, console.log(`Server running on ${port}`));

module.exports.app = app;