const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const _ = require('lodash');
const {ObjectId} = require('mongodb');
const timestamp = require('time-stamp');
const authenticate = require('../middleware/authenticate');
const currencyFormatter = require('currency-formatter');
const MarketSchema = require('../db/market-model');

const multer = require('multer');

const storage = multer.diskStorage({
    destination: function (req, file, cb){
        cb(null, 'image/market/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname.replace(' ', '_'));
    }
});

const fileFilter = (res, file, cb) => {

    if (file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 5
    }
});

router.get('/all', authenticate, (req, res, next) => {
    MarketSchema.find({})
        .exec()
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.send(err);
        });
});

router.get('/:id', authenticate, (req, res, next) => {
    
    var id = req.params.id;
    if (!ObjectId.isValid(id)){
        return res.json({
            message: "Invalid ID"
        });
    }else{
        MarketSchema.findById(id)
            .then(data => {
                 res.send(data);
            })
            .catch(err => {
                req.send(err);
            });
    }
});

router.delete('/:id', authenticate, (req, res, next) => {
    res.send('Market Delete');
});

router.post('/add', authenticate, upload.single('adImage'), (req, res, next) => {
    
    var body = _.pick(req.body, ['name', 'description', 'status', 'contact','categories']);
    var address = _.pick(req.body, ['city', 'area']);
    var placeBy = _.pick(req.body, ['_id', 'user', 'image']);
    var price = currencyFormatter.format(req.body.price, {code: 'SLL'});

    body._id = mongoose.Types.ObjectId(),
    body.address = address;
    
    body.placeBy = placeBy;

    body.price = price.replace('.00','') + ` My ${req.body.priceType}`;

    body.placeDate = timestamp('DD/MM/YYYY');

    body.adImage = ("http://178.128.36.198/" + req.file.path.replace(' ', '_'));
    
    var expireDate = new Date();
    
    expireDate.setDate(expireDate.getDate() + 15);

    body.expireDate = timestamp('DD/MM/YYYY',expireDate);
    
    var marketSchema = new MarketSchema(body);
    marketSchema.save()
        .then(ad => {
            res.send(ad);
        })
        .catch(err => {
            res.send(err);
        });
});

router.patch('/:id', authenticate, (req, res, next) => {
    res.send('Market update Ad');
});

module.exports = router;