const express = require('express');
const router = express.Router();
const _ = require('lodash');
const BankHistorySchema = require('../db/history');
const timestamp = require('time-stamp');

router.get('/:user_id', (req, res, next) => {
   
    var user_id = req.params.user_id;

    BankHistorySchema.find({user_id})
        .then(data => {
            res.json(data);
        })
        .catch(err => {
            req.send('No History');
        });
})

router.patch('/:_id', (req, res, next) => {

    var _id = req.params._id;
    var ad_id = req.body.ad_id;

    BankHistorySchema.findByIdAndUpdate({
        _id
    }, {
        $set: {
            ad_id
        }
    }, {
        new: true
    }).then(account => {
        res.status(200)
            .json({
                message: "Transaction Successful"
            });
    }).catch(err => {
        res.status(400)
            .send();
    });
});

module.exports = router;