const express = require('express');
const router = express.Router();
const _ = require('lodash');
const AccountSchema = require('../db/account-model');
const timestamp = require('time-stamp');
const BankHistorySchema = require('../db/history');

router.get('/check-balance/:userId', (req, res, next) => {
    var user_id = req.params.userId;

    AccountSchema.checkAccount(user_id)
        .then(balance => {
            res.json(balance);
        });
});

module.exports = router;