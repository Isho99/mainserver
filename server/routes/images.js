const express = require('express');
const router = express.Router();

router.get('/:imageId', (req, res, next) => {
    const ID = req.params.imageId;
    res.contentType('image/jpg');
    res.status(200).redirect(`/user/${ID}`);
});

router.get('/market/:imageId', (req, res, next) => {
    const ID = req.params.imageId;
    res.contentType('image/jpg');
    res.status(200).redirect(`/market/${ID}`);
});

router.get('/elite/:imageId', (req, res, next) => {
    const ID = req.params.imageId;
    res.contentType('image/jpg');
    res.status(200).redirect(`/elite/${ID}`);
});

module.exports = router;