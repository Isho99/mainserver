const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const _ = require('lodash');
const {
    ObjectId
} = require('mongodb');
const timestamp = require('time-stamp');
const authenticate = require('../middleware/authenticate');
const authenticateAccount = require('../middleware/authenticate-account');
const currencyFormatter = require('currency-formatter');
const request = require('request');
const EliteSchema = require('../db/elite-model.');
const multer = require('multer');
const AccountSchema = require('../db/account-model');
const BankHistorySchema = require('../db/history');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'image/elite/');
    },
    filename: function (req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname.replace(' ', '_'));
    }
});

const fileFilter = (res, file, cb) => {

    if (file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 5
    }
});

router.get('/all', authenticate, (req, res, next) => {
    EliteSchema.find({})
        .then(data => {
            res.json(data)
        })
        .catch(err => {
            res.send(err);
        });
});


router.get('/:id', authenticate, (req, res, next) => {

    var id = req.params.id;
    if (!ObjectId.isValid(id)) {
        return res.json({
            message: "Invalid ID"
        });
    } else {
        EliteSchema.findById(id)
            .then(data => {
                res.send(data);
            })
            .catch(err => {
                req.send(err);
            });
    }
});

router.post('/add', authenticateAccount, upload.any(), (req, res, next) => {

    var ad = _.pick(req.body, ['name', 'description', 'status', 'contact', 'adSubCategories']);
    var address = _.pick(req.body, ['city', 'area']);
    var placeBy = _.pick(req.body, ['_id', 'user', 'image']);
    var user_id = req.body._id;
    var adType = req.body.adType;
    var adPrice;
    var adSubCategories = ad.adSubCategories;

    adId = mongoose.Types.ObjectId(),
        ad._id = adId;
    ad.address = address;

    ad.placeBy = placeBy;

    if (req.body.priceType !== "Call Me for Details") {
        var price = currencyFormatter.format(req.body.price, {
            code: 'SLL'
        });
        ad.price = price.replace('.00', '') + ` My ${req.body.priceType}`;
    } else {
        ad.price = req.body.price;
    }

    ad.placeDate = timestamp('DD/MM/YYYY');

    for (var i = 1; i <= req.files.length; i++) {

        switch (i) {
            case 1:
                ad.adImage1 = ("http://178.128.36.198/" + req.files[0].path.replace(' ', '_'));
                break;
            case 2:
                ad.adImage2 = ("http://178.128.36.198/" + req.files[1].path.replace(' ', '_'));
                break;
            case 3:
                ad.adImage3 = ("http://178.128.36.198/" + req.files[2].path.replace(' ', '_'));
                break;
            case 4:
                ad.adImage4 = ("http://178.128.36.198/" + req.files[3].path.replace(' ', '_'));
                break;

            default:
                break;
        }

    }

    var expireDate = new Date();

    switch (adType) {
        case '15 Days':
            adPrice = 8;

            expireDate.setDate(expireDate.getDate() + 15);

            ad.expireDate = timestamp('DD/MM/YYYY', expireDate);
            break;

        case '1 Month':
            adPrice = 14;

            expireDate.setDate(expireDate.getDate() + 31);

            ad.expireDate = timestamp('DD/MM/YYYY', expireDate);
            break;

        default:
            break;
    }
    var price = currencyFormatter.format(req.body.price, {
        code: 'SLL'
    });

    var bank = {
        user_id,
        adCategories: 'Elite',
        adSubCategories,
        adPrice,
        adType,
        adId
    }

    AccountSchema.checkAccount(user_id)
        .then(data => {
            var arsabi_credit = data.arsabi_credit - adPrice;
            AccountSchema.findOneAndUpdate({
                    user_id
                }, {
                    $set: {
                        arsabi_credit
                    }
                }, {
                    new: true
                })
                .then(data => {
                    
                    new EliteSchema(ad)
                        .save()
                        .then(data => {
                             res.json(data);

                        }).catch();
                })
                .catch(err => {
                    res.send(err);
                })
        })
        .catch(err => {
            res.status(400)
                .send();
        });
});

module.exports = router;