const express = require('express');
const router = express.Router();
const _ = require('lodash');
const timestamp = require('time-stamp');
const ReportSchema = require('../db/Report-model');

router.get('/', (req, res, next) => {

});

router.post('/', (req, res, next) => {
    const body = _.pick(req.body, ['report', 'user', 'adID', 'adImage1', 'adImage2', 'adImage3', 'adImage4', 'adImage5', 'adImage6', 'adImage7', 'adImage8', 'adImage9', 'adImage10']);
    
    var date = new Date();
    body.reportDate = timestamp('DD-MM-YYYY', date);

    var report = new ReportSchema(body);
    report.save()
        .then(report => {
            res.send(report);
        })
        .catch(err => {
            res.send(err);
        });
});

module.exports = router;