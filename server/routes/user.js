const express = require('express');
const router = express.Router();
const _ = require('lodash')
const mongoose = require('mongoose');
const authenticate = require('../middleware/authenticate');
const BankSchema = require('../db/account-model');
const request = require('request');

const {
    ObjectID
} = require('mongodb');
const multer = require('multer');
const User = require('../db/user-model');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'image/user/');
    },
    filename: function (req, file, cb) {
        var name;
        if (file.mimetype === 'image/jpeg') {
            name = '.jpeg';
        } else if (file.mimetype === 'image/png') {
            name = '.png'
        } else if (file.mimetype === 'image/jpeg') {
            name = '.jpg';
        }

        cb(null, req.params.id + name);
    }
})

const fileFilter = (res, file, cb) => {

    if (file.mimetype === 'image/jpeg' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

const upload = multer({
    storage: storage,
    fileFilter: fileFilter,
    limits: {
        fileSize: 1024 * 1024 * 5
    }
});

router.post('/', (req, res, next) => {
    var body = _.pick(req.body, ['user', 'password']);
    User.findByCredentials(body.user, body.password)
        .then((user) => {
            return user.generateAuthToken()
                .then(token => {
                    res.header('x-auth', token)
                        .json({
                            token: token,
                            phone: user.phone,
                            user: user.user,
                            image: user.image,
                            _id: user._id,
                            message: "Successfully Login"
                        });
                })
                .catch(error => {
                    res.status(400)
                        .send();
                });
        })
        .catch(error => {
            res.json({
                message: error
            });
        });
});

router.post('/register', (req, res, next) => {
    var body = _.pick(req.body, ['name', 'user', 'phone', 'password']);

    body._id = mongoose.Types.ObjectId();

    var user = new User(body);
    user.save()
        .then((user) => {
            return user.generateAuthToken();
        })
        .then(token => {
            createAccount(res, body._id, user, token)
        }).catch(error => {
            res.json({
                message: error
            });
        });
});

router.patch('/:id', authenticate, upload.single('userImage'), (req, res, next) => {
    var key = req.params.id;
    var address = _.pick(req.body, ['city', 'location']);
    var details = _.pick(req.body, ['fName', 'lName', 'title']);
    var body = {
        address: address,
        image: "http://178.128.36.198/" + req.file.path.replace('image', ''),
        first_name: details.fName,
        last_name: details.lName,
        title: details.title
    }

    if (!ObjectID.isValid(key)) {
        return res.json({
            message: "Invalid ID"
        });
    }

    User.findByIdAndUpdate(key, {
            $set: body
        }, {
            new: true
        })
        .then(user => {
            if (!user) {
                return res.status(404).send();
            }

            res.json(user);
        }).catch(err => {
            res.json(err);
        });
});

function createAccount(res, user_id, user, token) {
    var body = {
        user_id,
        arsabi_credit: 20
    }

    new BankSchema(body)
        .save()
        .then(data => {
            res.header('x-auth', token).json({
                token: token,
                phone: user.phone,
                _id: user._id,
                user: user.user
            })
        })
        .catch(err => {
            res.status(400).send();
        });
}

module.exports = router;