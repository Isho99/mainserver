const jwt = require('jsonwebtoken');
const User = require('../db/user-model');
const _ = require('lodash');
const request = require('request');
const AccountSchema = require('../db/account-model');

module.exports = (req, res, next) => {
    var token = req.header('x-auth');
    var id = req.header('id');
    var price = req.header('price');

    User.findByToken(token)
        .then((user) => {
            if (!user) {}

            AccountSchema.checkAccount(id)
                .then(data => {
                    if (data.arsabi_credit >= price){
                        next();
                    }else{
                        res.json({
                            arsabi_credit: data.arsabi_credit,
                            message: "Sorry you have Insufficent Balance\nto Place the Ad"
                        });
                    }
                })
                .catch(err => {
                    res.status(400)
                        .send();
                });
        })
        .catch(error => {
            res.status(401).json({
                message: error
            });
        });
}