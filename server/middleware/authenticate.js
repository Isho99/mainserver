const jwt = require('jsonwebtoken');
const User = require('../db/user-model');

module.exports = (req, res, next) => {
    var token = req.header('x-auth');

    User.findByToken(token)
        .then((user) => {
            if (!user) {
            }
            
            next();
        })
        .catch(error => {
            res.status(401).json({
                message: error
            });
        });
}