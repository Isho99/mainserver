const mongose = require('mongoose');
const _ = require('lodash');

const MarketSchema = mongose.Schema({
    _id: mongose.Schema.Types.ObjectId,
    name: {type: String, required: true},
    price: {type: String, required: true},
    adImage: {type: String, required: true},
    status: {type: String, required: true},
    categories: {type: String, required: true},
    description: {type: String, required: true},
    address: {
        city: {type: String, required: true},
        area: {type: String, required: true}
    },

    placeBy: {
        _id: {type: String, required: true},
        user: {type: String, required: true},
        image: {type: String, required: true}
    },
    contact: {type: Number, required: true},
    placeDate: {type: String, required: true},
    expireDate: {type: String, required: true}
});

module.exports = mongose.model("Market", MarketSchema);