const mongoose = require('mongoose');

const ReportSchema = mongoose.Schema({
    report: {type: String, required: true},
    user: {type: String, required: true},
    adID: {type: String, required: true},
    adImage1: {type: String},
    adImage2: {type: String},
    adImage3: {type: String},
    adImage4: {type: String},
    adImage5: {type: String},
    adImage6: {type: String},
    adImage7: {type: String},
    adImage8: {type: String},
    adImage9: {type: String},
    adImage10: {type: String},
    reportDate: {type: String, required: true}
});

module.exports = mongoose.model('ReportAd', ReportSchema);





