const mongose = require('mongoose');
const _ = require('lodash');

const EliteSchema = mongose.Schema({
    _id: mongose.Schema.Types.ObjectId,
    name: {type: String, required: true},
    price: {type: String, required: true},
    adImage1: {type: String, required: true},
    adImage2: {type: String},
    adImage3: {type: String},
    adImage4: {type: String},
    status: {type: String, required: true},
    adSubCategories: {type: String, required: true},
    description: {type: String, required: true},
    address: {
        city: {type: String, required: true},
        area: {type: String, required: true}
    },

    placeBy: {
        _id: {type: String, required: true},
        user: {type: String, required: true},
        image: {type: String, required: true}
    },
    contact: {type: Number, required: true},
    placeDate: {type: String, required: true},
    expireDate: {type: String, required: true}
});

module.exports = mongose.model("Elite", EliteSchema);