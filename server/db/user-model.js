const mongoose = require('mongoose');
const _ = require('lodash');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const UserSchame = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: {type: String},
    first_name: {type: String},
    last_name: {type: String},
    user: {type: String, required: true, unique: true},
    address: {
        city: {type: String},
        location: {type: String}
    },
    image: {type: String},
    phone: {type: Number, required: true, unique: true},
    password: {type: String, required: true},
    tokens: [{
        access: {type: String,require: true},
        token: {type: String,require: true}
    }]
});

//Customize the output
UserSchame.methods.toJSON = function(){
    var user = this;
    var userObject = user.toObject();
    return _.pick(userObject, ['_id','phone', 'user', 'image', 'address']);
}

//Generate Authentaction token for a user
UserSchame.methods.generateAuthToken = function () {
    var user = this;
    var access = "auth";
    var token = jwt.sign({
        _id: user._id.toHexString()
    }, 'arsabi01').toString();

    user.tokens.push({
        access,
        token
    });

    return user.save()
        .then(() => {
            return token
        });
}

//Cheack if a exit in our database
UserSchame.statics.findByCredentials = function (user, password){

    var User = this;
    return User.findOne({user})
        .then(user => {

            if(!user){
                return Promise.reject("Invalid user");
            }

            return new Promise((resolve, reject) => {
                bcrypt.compare(password, user.password, (err, res) => {
                    if (res){
                        resolve(user);
                    }else{
                        reject("Incorrect password");
                    }
                });
            });
        });
}

//Hash password after save
UserSchame.pre('save', function(next) {

    var user = this;

    if (user.isModified('password')){
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(user.password, salt, (err, hash) => {
                user.password = hash;
                next();
            });
        });
    }else{
        next();
    }
});

UserSchame.statics.findByToken = function (token) {
    var User = this;
    var decoded;

    try {
        decoded = jwt.verify(token, 'arsabi01')
    } catch (error) {
        return Promise.reject('401 Error Unauthorized User');
    }

    return User.findOne({
        '_id': decoded._id,
        'tokens.token': token,
        'tokens.access': 'auth'
    });
}

module.exports = mongoose.model('User', UserSchame);