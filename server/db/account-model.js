const mongoose = require('mongoose');
const timestamp = require('time-stamp');
const _ = require('lodash');

const AccountSchema = mongoose.Schema({
    user_id: {type: String, required: true},
    arsabi_credit: {type: Number, required: true}
});

AccountSchema.methods.toJSON = function() {
    var account = this;
    var accountObject = account.toObject();
    return _.pick(accountObject, ['user_id', '_id', 'arsabi_credit']);
}

AccountSchema.statics.checkAccount = function (user_id) {
    
    var Account = this;
    return Account.findOne({user_id})
        .then(account => {
            var arsabi_credit = _.pick(account, ['arsabi_credit']);
            return Promise.resolve(arsabi_credit);
        });
}

module.exports = mongoose.model('Account', AccountSchema);